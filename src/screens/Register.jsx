import {StyleSheet, Text, View, TextInput, ScrollView, Image, TouchableOpacity} from "react-native";
import React, {useState} from "react";
import Ionicons from "@expo/vector-icons/Ionicons";
import {colors} from "../utils/theme";

export default function Register() {
    

    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [userGender, setUserGender] = useState();

    const [showPass, setShowPass] = useState(true);

    function onEyePress() {
        if (showPass === true) {
            setShowPass(false);
        } else {
            setShowPass(true);
        }
    }

    function onRegisterPress(){
        console.log({
            firstName, lastName, email, password, userGender,
        } );
        
    }


    return (
            <ScrollView>
            <View style={styles.formCon}>
                <Image style= {{width:100, height:100, alignSelf:"center"}} source={require("../../assets/logo.jpg")} />
                <Image style= {{width:100, height: 100, alignSelf:"center"}} source = {{uri: "https://cdn.pixabay.com/photo/2016/11/19/09/44/antique-1838324_960_720.jpg"}} />
                <TextInput placeholder= {"First Name"} onChangeText={setFirstName} style = {styles.inputCon} />
                <TextInput placeholder= {"Last Name"} onChangeText={setLastName} style = {styles.inputCon} />
                <TextInput placeholder={"email"} onChangeText={setEmail} style={styles.inputCon} />
                {/* <TextInput placeholder={"password"} onChangeText={setPassword} style={styles.inputCon} secureTextEntry={true} /> */}
                <View style={styles.passwordCon}>
                    <TextInput placeholder="password" onChangeText={setPassword} secureTextEntry={showPass} />
                    <Ionicons 
                        name={showPass === true ? "eye-off" : "eye"}
                        color={showPass === true ? colors.accent : "red"}
                        size={18}
                        onPress={onEyePress}
                    />
                </View>
                <TextInput placeholder="Gender" style={styles.inputCon} onChangeText={setUserGender} />
                <TouchableOpacity style= {styles.btn} onPress={onRegisterPress}>
                <Text style= {styles.btnText}>Register</Text>
                </TouchableOpacity>
                
            </View>
            </ScrollView>
    )
}

const styles = StyleSheet.create({
    formCon: {
        width: "100%",
        marginTop: 200,
        padding: 10,

    },
    inputCon: {
        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 10,
        padding: 10,
        marginVertical: 5,
    },
    btn: {
        backgroundColor: colors.primary,
        padding: 10,
        borderRadius: 5,
        width: "60%",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        
    },
    btnText: {
        color: colors.white,

    },
    passwordCon: {
        borderWidth: 1,
        borderColor: colors.primary,
        padding: 10,
        borderRadius: 10,
        marginVertical: 5,
        flexDirection:"row",
        alignItems:"center",
        justifyContent: "space-between",
    }
});